require "sinatra"
require "sinatra/reloader" if development?
require "sinatra/activerecord"
require "sinatra/base"
require "yajl"

require "./app/controllers/application_controller"
require "./app/controllers/status_controller"
require "./app/controllers/search_controller"
require "./app/controllers/providers_controller"

require "./app/models/provider"
require "./app/models/rate"

require "./app/adapters/european_central_bank_adapter"

require "./app/services/create_rate_for_provider"
require "./app/services/load_provider_data"
require "./app/services/calculate_rate"

require "./app/factories/loader_factory"

require "./app/loaders/european_central_bank_loader"

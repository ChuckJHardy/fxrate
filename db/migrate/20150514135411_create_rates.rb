class CreateRates < ActiveRecord::Migration
  def change
    create_table :rates do |t|
      t.references :provider

      t.datetime :date, null: false
      t.string :currency, null: false
      t.decimal :rate, precision: 15, scale: 10
    end

    add_index(:rates, :provider_id)
    add_index(:rates, :currency)
  end
end

class CreateProviders < ActiveRecord::Migration
  def change
    create_table :providers do |t|
      t.string :identifier, null: false
    end

    add_index(:providers, :identifier)
  end
end

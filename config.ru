ENV["RACK_ENV"] ||= "development"
ENV["DATABASE_URL"] ||= ENV["POSTGRES_URL"]

require "./application"

run FXRate::Application

require "spec_helper"

require "database_cleaner"

ActiveRecord::Base.logger = nil

ActiveRecord::Base.establish_connection(
  ENV["DATABASE_URL"] ||= ENV["POSTGRES_URL_TEST"]
)

RSpec.configure do |config|
  config.before do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
end

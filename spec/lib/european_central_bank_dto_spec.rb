require "./lib/european_central_bank_dto"

RSpec.describe FXRate::EuropeanCentralBankDTO, type: :dto do
  subject(:dto) { described_class.fetch }

  let(:results) { { rate: 1 } }
  let(:data) do
    {
      'Envelope' => {
        'Cube' => {
          'Cube' => results
        }
      }
    }
  end

  it "sets expected base uri" do
    expect(described_class.base_uri).to eq(
      "http://www.ecb.europa.eu/stats/eurofxref"
    )
  end

  it "returns rate hash" do
    expect(described_class).to receive(:get)
      .with("/eurofxref-hist-90d.xml") { data }

    expect(dto).to eq(results)
  end
end

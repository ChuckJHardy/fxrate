module Factory
  def build_provider(overrides = {})
    FXRate::Provider.new(provider_attributes(overrides))
  end

  def create_provider(overrides = {})
    build_provider(overrides).save!
  end

  def provider_attributes(overrides = {})
    {
      identifier: :test_provider,
    }.merge!(overrides)
  end

  def build_rate(overrides = {})
    FXRate::Rate.new(rate_attributes(overrides))
  end

  def create_rate(overrides = {})
    build_rate(overrides).save!
  end

  def rate_attributes(overrides = {})
    {
      provider: build_provider,
      date: "2015-01-02",
      currency: "GBP",
      rate: "191.23481"
    }.merge!(overrides)
  end
end

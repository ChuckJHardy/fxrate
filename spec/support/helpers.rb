module Helpers
  def parsed_response(body)
    Yajl::Parser.parse(body, symbolize_keys: true)
  end
end

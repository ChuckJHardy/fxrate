require "spec_helper_active_record"

RSpec.describe "Application", type: :request do
  describe "GET '/'" do
    subject(:endpoint) { get "/" }

    let(:expected_response) { { status: "Awesomesauce" } }

    it "returns expected status" do
      endpoint
      expect(last_response).to be_ok
    end

    it "returns expected response" do
      endpoint
      expect(parsed_response(last_response.body)).to eq(expected_response)
    end
  end

  describe "GET '/search'" do
    subject(:endpoint) { get "/search", query }

    let!(:provider) { build_provider(identifier: identifier) }
    let!(:date) { Date.today }

    let(:identifier) { :european_central_bank }

    let(:base_currency) { "GBP" }
    let(:counter_currency) { "USD" }

    let!(:rate_gbp) do
      create_rate(
        provider: provider,
        date: date,
        currency: base_currency,
        rate: "17.181"
      )
    end

    let!(:rate_usd) do
      create_rate(
        provider: provider,
        date: date,
        currency: counter_currency,
        rate: "0.817"
      )
    end

    context "Success" do
      let(:query) do
        {
          provider: identifier,
          date: date,
          base: base_currency,
          counter: counter_currency
        }
      end

      let(:expected_response) { { rate: "21.029375764993880049" } }

      it "returns expected status" do
        endpoint
        expect(last_response).to be_ok
      end

      it "returns expected response" do
        endpoint
        expect(parsed_response(last_response.body)).to eq(expected_response)
      end
    end

    context "Failure" do
      let(:query) do
        {
          provider: identifier,
          date: date,
          base: "AAA",
          counter: counter_currency
        }
      end

      let(:expected_response) { { error: "Invalid Search" } }

      it "returns expected status" do
        endpoint
        expect(last_response.status).to eq(404)
      end

      it "returns expected response" do
        endpoint
        expect(parsed_response(last_response.body)).to eq(expected_response)
      end
    end
  end

  describe "GET '/providers/:provider/dates'" do
    subject(:endpoint) { get "/providers/european_central_bank/dates" }

    let!(:provider) { build_provider(identifier: :european_central_bank) }
    let(:expected_response) { { dates: [ "2015-01-02" ] } }

    before do
      create_rate(provider: provider)
    end

    it "returns expected status" do
      endpoint
      expect(last_response).to be_ok
    end

    it "returns expected response" do
      endpoint
      expect(parsed_response(last_response.body)).to eq(expected_response)
    end
  end

  describe "GET '/providers/:provider/currencies'" do
    subject(:endpoint) { get "/providers/european_central_bank/currencies" }

    let!(:provider) { build_provider(identifier: :european_central_bank) }
    let(:expected_response) { { currencies: [ "GBP" ] } }

    before do
      create_rate(provider: provider)
    end

    it "returns expected status" do
      endpoint
      expect(last_response).to be_ok
    end

    it "returns expected response" do
      endpoint
      expect(parsed_response(last_response.body)).to eq(expected_response)
    end
  end
end

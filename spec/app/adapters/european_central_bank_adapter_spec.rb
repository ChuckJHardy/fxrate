RSpec.describe FXRate::EuropeanCentralBankAdapter, type: :adapter do
  subject(:adapter) { described_class.adapt(data) }

  let(:data) do
    [
      {
        "time" => "2015-05-13",
        "Cube" => [
          { "currency" => "USD", "rate" => "1.9181" },
          { "currency" => "JPY", "rate" => "131.16" }
        ]
      },
      {
        "time" => "2015-05-12",
        "Cube" => [
          { "currency" => "USD", "rate" => "1.1239" },
          { "currency" => "JPY", "rate" => "134.85" }
        ]
      }
    ]
  end

  let(:adapted_hash) do
    [
      { date: "2015-05-13", currency: "USD", rate: 1.9181 },
      { date: "2015-05-13", currency: "JPY", rate: 131.16 },
      { date: "2015-05-12", currency: "USD", rate: 1.1239 },
      { date: "2015-05-12", currency: "JPY", rate: 134.85 }
    ]
  end

  it "returns expected adapted hash" do
    expect(adapter).to eq(adapted_hash)
  end
end

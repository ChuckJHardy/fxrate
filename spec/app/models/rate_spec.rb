require "spec_helper_active_record"

RSpec.describe FXRate::Rate, type: :model do
  subject(:model) { described_class.new(params) }

  let(:params) { rate_attributes }

  describe ".by_date_and_currency" do
    subject(:scope) { described_class.by_date_and_currency(date, currency) }

    let(:date) { "2015-01-02" }
    let(:currency) { "GBP" }

    it "returns calls active record with expected sql" do
      scope.to_sql.tap do |sql|
        expect(sql).to include(Date.parse(date).to_s)
        expect(sql).to include(currency)
      end
    end
  end

  describe ".unique_dates" do
    before { create_rate }

    it "returns collection of date strings" do
      expect(described_class.unique_dates).to eq(["2015-01-02"])
    end
  end

  describe ".unique_currencies" do
    before { create_rate }

    it "returns collection of unique currencies" do
      expect(described_class.unique_currencies).to eq(["GBP"])
    end
  end

  describe "#provider" do
    it "returns provider" do
      expect(model.provider).to be_an_instance_of(FXRate::Provider)
    end
  end

  describe "#date" do
    it "returns assigned date" do
      expect(model.date).to eq(params[:date])
    end
  end

  describe "#currency" do
    it "returns assigned currency" do
      expect(model.currency).to eq(params[:currency])
    end
  end

  describe "#rate" do
    it "returns BigDecimal" do
      expect(model.rate).to be_an_instance_of(BigDecimal)
    end

    it "returns assigned rate" do
      expect(model.rate.to_s).to eq(params[:rate])
    end
  end
end

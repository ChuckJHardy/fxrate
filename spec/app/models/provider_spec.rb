require 'spec_helper_active_record'

RSpec.describe FXRate::Provider, type: :model do
  subject(:model) { described_class.new(provider) }

  let!(:provider) { provider_attributes }

  describe "#identifier" do
    it "returns assigned identifier" do
      expect(model.identifier).to eq(provider[:identifier].to_s)
    end
  end

  describe "#rates" do
    it "returns collection" do
      expect(model.rates).to eq([])
    end
  end
end

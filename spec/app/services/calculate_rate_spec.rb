RSpec.describe FXRate::CalculateRate, type: :service do
  subject(:service) { described_class.call(query: query) }

  let!(:provider) { build_provider(identifier: identifier) }
  let!(:date) { Date.today }

  let(:base_currency) { "GBP" }
  let(:counter_currency) { "USD" }
  let(:identifier) { :european_central_bank }

  let(:query) do
    {
      "provider" => identifier,
      "date" => date,
      "base" => base_currency,
      "counter" => counter_currency
    }
  end

  let(:provider) { instance_double('FXRate::Provider', rates: double) }
  let(:rates) { [double('FXRate::Rate', rate: 0.8)] }

  before do
    allow(FXRate::Provider).to receive(:find_by)
      .with(identifier: identifier) { provider }

    expect(provider.rates).to receive(:by_date_and_currency)
      .with(date, base_currency) { rates }
  end

  context "when valid" do
    it "returns exchange rate" do
      expect(provider.rates).to receive(:by_date_and_currency)
        .with(date, counter_currency) { rates }

      expect(service).to eq(1)
    end
  end

  context "when invalid" do
    let(:rates) { [] }

    it "returns nil" do
      expect(service).to be_nil
    end
  end
end

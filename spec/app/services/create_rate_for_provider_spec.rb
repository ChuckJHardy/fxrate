require "spec_helper_active_record"

RSpec.describe FXRate::CreateRateForProvider, type: :service do
  subject(:service) { described_class.call(provider: provider, data: rate_data) }

  let!(:provider) { build_provider }
  let!(:rate_data) { rate_attributes(provider: provider) }

  context "when rate does not exist" do
    it "creates and returns rate" do
      expect { service }.to change { FXRate::Rate.count }.by(1)
    end
  end

  context "when rate exists" do
    it "returns rate" do
      service
      expect { service }.to change { FXRate::Rate.count }.by(0)
    end
  end
end

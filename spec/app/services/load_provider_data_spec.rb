require "spec_helper_active_record"

RSpec.describe FXRate::LoadProviderData, type: :service do
  subject(:service) { described_class.call(identifier: identifier, data: data) }

  let(:identifier) { :european_central_bank }
  let(:data) do
    [
      { date: "2015-05-13", "currency" => "USD", "rate" => "1.9181" },
      { date: "2015-05-13", "currency" => "JPY", "rate" => "131.16" }
    ]
  end

  it "creates provider" do
    expect { service }.to change { FXRate::Provider.count }.by(1)
  end

  it "calls the CreateRateForProvider once for each data record" do
    expect(FXRate::CreateRateForProvider).to receive(:call)
      .with(provider: an_instance_of(FXRate::Provider), data: data[0])

    expect(FXRate::CreateRateForProvider).to receive(:call)
      .with(provider: an_instance_of(FXRate::Provider), data: data[1])

    service
  end
end

describe FXRate::ApplicationController do
  let(:instance) { described_class.new(params: params) }
  let(:params) { {} }

  describe '.index' do
    subject { described_class.index }

    it 'returns nil' do
      expect(subject).to be_nil
    end
  end

  describe '.show' do
    subject { described_class.show }

    it 'returns nil' do
      expect(subject).to be_nil
    end
  end
end

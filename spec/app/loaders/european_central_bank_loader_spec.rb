RSpec.describe FXRate::EuropeanCentralBankLoader, type: :loader do
  subject(:loader) { described_class.load }

  let(:dto_data) { { from: :dto } }
  let(:adapted_data) { { from: :adapter } }

  it "calls LoadProviderData service with expected args" do
    expect(FXRate::EuropeanCentralBankDTO).to receive(:fetch) { dto_data }

    expect(FXRate::EuropeanCentralBankAdapter).to receive(:adapt)
      .with(dto_data) { adapted_data }

    expect(FXRate::LoadProviderData).to receive(:call)
      .with(identifier: :european_central_bank, data: adapted_data)

    loader
  end
end

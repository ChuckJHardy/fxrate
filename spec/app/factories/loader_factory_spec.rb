RSpec.describe FXRate::LoaderFactory, type: :service do
  subject(:factory) do
    described_class.by_provider_identifier(identifier)
  end

  let(:identifier) { :european_central_bank }

  context "when loader exists" do
    it "calls providers loader" do
      expect(FXRate::EuropeanCentralBankLoader).to receive(:load)
      factory
    end
  end

  context "when loader does not exist" do
    let(:identifier) { :will_not_exist }

    it "raises an error" do
      expect { factory }.to raise_error(FXRate::LoaderFactory::LoaderNotFound)
    end
  end
end

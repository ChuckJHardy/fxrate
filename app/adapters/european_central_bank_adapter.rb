module FXRate
  class EuropeanCentralBankAdapter
    def initialize(data:)
      @data = data
    end

    def self.adapt(data)
      new(data: data).adapt
    end

    def adapt
      @data.flat_map(&Mapper)
    end

    private

    class Mapper
      def initialize(record:)
        @record = record
      end

      def self.to_proc
        ->(record) { new(record: record).as_hash }
      end

      def as_hash
        rates.flat_map(&rate)
      end

      private

      def rate
        ->(rate) { Rate.adapt(date: date, rate: rate) }
      end

      def date
        @record.fetch("time")
      end

      def rates
        @record.fetch("Cube")
      end

      class Rate
        attr_reader :rate

        def initialize(date:, rate:)
          @date = date
          @rate = rate
        end

        def self.adapt(*args)
          new(*args).adapt
        end

        def adapt
          {
            date: @date,
            currency: rate.fetch("currency"),
            rate: rate_as_decimal
          }
        end

        private

        def rate_as_decimal
          rate.fetch("rate", 0).to_f
        end
      end
    end
  end
end

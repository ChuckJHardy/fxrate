module FXRate
  class SearchController < ApplicationController
    def show
      if rate
        [200, { rate: rate }.to_json]
      else
        [404, { error: "Invalid Search" }.to_json]
      end
    end

    private

    def rate
      @rate ||= FXRate::CalculateRate.call(query: params)
    end
  end
end

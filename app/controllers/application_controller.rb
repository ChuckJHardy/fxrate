module FXRate
  class ApplicationController
    attr_reader :params

    def initialize(params: {})
      @params = params
    end

    def self.index(*args)
      new(*args).index
    end

    def self.show(*args)
      new(*args).show
    end

    def index; end
    def show; end
  end
end

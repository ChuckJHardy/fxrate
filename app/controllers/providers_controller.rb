module FXRate
  class ProvidersController < ApplicationController
    def self.dates(*args)
      new(*args).dates
    end

    def self.currencies(*args)
      new(*args).currencies
    end

    def dates
      [200, { dates: provider.rates.unique_dates }.to_json]
    end

    def currencies
      [200, { currencies: provider.rates.unique_currencies }.to_json]
    end

    private

    def provider
      @provider ||= FXRate::Provider.find_by(identifier: params["provider"])
    end
  end
end

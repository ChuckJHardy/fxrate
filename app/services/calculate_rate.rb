module FXRate
  class CalculateRate
    def initialize(query:)
      @query = query
    end

    def self.call(*args)
      new(*args).call
    end

    def call
      base.rate / counter.rate if valid?
    end

    protected

    attr_reader :query

    private

    def valid?
      base && counter
    end

    def base
      @base ||= rates_for("base")
    end

    def counter
      @counter ||= rates_for("counter")
    end

    def date
      query.fetch("date") { Date.today }
    end

    def rates_for(key)
      provider.rates.by_date_and_currency(date, query[key]).first
    end

    def provider
      @provider ||= FXRate::Provider.find_by(identifier: query["provider"])
    end
  end
end

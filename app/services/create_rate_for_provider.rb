module FXRate
  class CreateRateForProvider
    def initialize(provider:, data:)
      @provider = provider
      @data = data
    end

    def self.call(*args)
      new(*args).call
    end

    def call
      existing_rates.find_or_create_by(data)
    end

    private

    def data
      @data.merge(provider: @provider)
    end

    def existing_rates
      @provider.rates.by_date_and_currency(data[:date], data[:currency])
    end
  end
end

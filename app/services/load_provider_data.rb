module FXRate
  class LoadProviderData
    def initialize(identifier:, data:)
      @identifier = identifier
      @data = data
    end

    def self.call(*args)
      new(*args).call
    end

    def call
      @data.each(&first_or_create)
    end

    private

    def first_or_create
      ->(rate) {
        FXRate::CreateRateForProvider.call(provider: provider, data: rate)
      }
    end

    def provider
      @provider ||= FXRate::Provider.first_or_create(identifier: @identifier)
    end
  end
end

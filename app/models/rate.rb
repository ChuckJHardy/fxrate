module FXRate
  class Rate < ActiveRecord::Base
    belongs_to :provider

    scope :by_date_and_currency, ->(date, currency) do
      where(date: Date.parse(date.to_s), currency: currency)
    end

    def self.unique_dates
      pluck(:date).uniq.map { |date| date.to_date.to_s }
    end

    def self.unique_currencies
      pluck(:currency).uniq
    end
  end
end

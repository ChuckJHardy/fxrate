module FXRate
  class Provider < ActiveRecord::Base
    has_many :rates
  end
end

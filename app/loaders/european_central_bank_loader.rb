require "./lib/european_central_bank_dto"

module FXRate
  class EuropeanCentralBankLoader
    def self.load
      new.load
    end

    def load
      FXRate::LoadProviderData.call(
        identifier: identifier,
        data: adapted_data
      )
    end

    private

    def identifier
      :european_central_bank
    end

    def adapted_data
      FXRate::EuropeanCentralBankAdapter.adapt(data)
    end

    def data
      FXRate::EuropeanCentralBankDTO.fetch
    end
  end
end

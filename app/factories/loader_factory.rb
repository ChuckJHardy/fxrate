module FXRate
  class LoaderFactory
    LoaderNotFound = Class.new(StandardError)

    def initialize(identifier:)
      @identifier = identifier
    end

    def self.by_provider_identifier(identifier)
      new(identifier: identifier).call
    end

    def call
      FXRate.const_get(loader).load
    rescue NameError
      fail LoaderNotFound
    end

    private

    def loader
      "#{@identifier}_loader".classify
    end
  end
end

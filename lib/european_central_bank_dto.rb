require "httparty"

module FXRate
  class EuropeanCentralBankDTO
    include HTTParty

    base_uri "http://www.ecb.europa.eu/stats/eurofxref"

    def self.fetch
      new.fetch
    end

    def fetch
      data["Envelope"]["Cube"]["Cube"]
    end

    private

    def data
      self.class.get("/eurofxref-hist-90d.xml")
    end
  end
end


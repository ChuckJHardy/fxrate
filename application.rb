require "./config/load"

module FXRate
  class Application < Sinatra::Base
    # :nocov:
    register Sinatra::ActiveRecordExtension

    configure :development do
      register Sinatra::Reloader
    end

    before do
      content_type :json, charset: "utf-8"
    end
    # :nocov:

    get "/" do
      StatusController.index
    end

    get "/search" do
      SearchController.show(params: params)
    end

    get "/providers/:provider/dates" do
      ProvidersController.dates(params: params)
    end

    get "/providers/:provider/currencies" do
      ProvidersController.currencies(params: params)
    end
  end
end

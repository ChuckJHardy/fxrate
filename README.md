# FX Rate

Retrieve the foreign exchange rate from base to counter currency from a specific provider on a given day.

### Installation

Install dependencies:

[Direnv](https://github.com/zimbatm/direnv) or alternative for managing environment variables.

    $ brew install direnv

Copy example environment variables file:

    $ cp .envrc.example .envrc

Execute:

    $ bin/setup

### Usage

Load Data for a Provider:

    $ bin/rake load:for_provider[european_central_bank]

Start development server:

    $ bin/server

[Download](./postman_collection) and add the Postman File and required environments.

Environment | Key | Value
--- | ----- | -----------
Local | url | `http://localhost:9299`

### Testing

Run Unit Tests:

    $ bin/rspec

### Todo

Future Tasks

- [ ] List available loaders
- [ ] Data cleanup
- [ ] List available loaders
- [ ] Better error handling for Search
- [ ] Switch to Unicorn or Puma
